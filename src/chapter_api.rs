use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use crate::Language;

/// Whether long-strip mode is default
#[derive(Debug, Serialize_repr, Deserialize_repr, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
#[repr(u8)]
pub enum LongStrip {
	/// Not long-strip (boolean false)
	No = 0,
	/// Long strip (boolean true)
	Yes = 1,
}
impl From<bool> for LongStrip {
	fn from(b: bool) -> Self { if b { LongStrip::Yes } else { LongStrip::No } }
}
impl From<LongStrip> for bool {
	fn from(b: LongStrip) -> Self {
		match b {
			LongStrip::No => false,
			LongStrip::Yes => true,
		}
	}
}

/// Information on chapters that are currently available
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexChapterAPIChapterData {
	/// Server on which it's hosted (/data/ => https://mangadex.org/data/)
	pub server: String,
	/// Hash of the data
	pub hash: String,
	/// Array of pages
	pub page_array: Vec<String>,
}

/// Information on a manga chapter
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexChapterAPIChapter {
	pub id: u32,
	pub timestamp: u64,
	/// Parent manga (prefix: https://mangadex.org/api/manga)
	pub manga_id: u32,

	pub volume: String,
	pub chapter: String,
	pub title: String,

	#[serde(rename(deserialize = "lang_code"))]
	pub lang: Language,
	pub comments: Option<u32>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexMangaAPIGroupIDs {
	pub group_id: u32,
	pub group_id_2: u32,
	pub group_id_3: u32,
}

/// Chapter API data when delayed
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexChapterAPIDelayed {
	/// Group website, if available.
	#[serde(default)]
	pub group_website: String,
	/// Information
	#[serde(flatten)]
	pub info: MangaDexChapterAPIChapter,
	/// Groups contributing
	#[serde(flatten)]
	pub groups: MangaDexMangaAPIGroupIDs,
}
/// Chapter API data when unavailable
/// Note that it does not include the group id.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexChapterAPIUnavailable(MangaDexChapterAPIChapter);

/// Chapter API data when OK
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexChapterAPIOK {
	/// Pages
	#[serde(flatten)]
	pub pages: MangaDexChapterAPIChapterData,
	/// Information
	#[serde(flatten)]
	pub info: MangaDexChapterAPIChapter,
	/// Contributing groups
	#[serde(flatten)]
	pub groups: MangaDexMangaAPIGroupIDs,
	pub long_strip: LongStrip,
}

/// Information from the mangadex chapter api (/api/chapter/2; .)
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "status")]
pub enum MangaDexChapterAPI {
	/// When the chapter is deleted by the user or a moderator
	#[serde(rename(deserialize = "deleted"))]
	Deleted { id: u32 },
	/// When the chapter has a group delay
	#[serde(rename(deserialize = "delayed"))]
	Delayed(Box<MangaDexChapterAPIDelayed>),
	/// When the chapter is unavailable
	#[serde(rename(deserialize = "unavailable"))]
	Unavailable(Box<MangaDexChapterAPIUnavailable>),
	/// When the chapter is available
	#[serde(rename(deserialize = "OK"))]
	Ok(Box<MangaDexChapterAPIOK>),
	#[serde(rename(deserialize = "error"))]
	Error(Error),
}
impl MangaDexChapterAPI {
	pub fn is_ok(&self) -> bool {
		match self {
			MangaDexChapterAPI::Ok(_) => true,
			_ => false
		}
	}
	pub fn is_deleted(&self) -> bool {
		match self {
			MangaDexChapterAPI::Deleted { .. } => true,
			_ => false
		}
	}
	pub fn is_delayed(&self) -> bool {
		match self {
			MangaDexChapterAPI::Delayed(_) => true,
			_ => false,
		}
	}
	pub fn is_error(&self) -> bool {
		match self {
			MangaDexChapterAPI::Error { .. } => true,
			_ => false
		}
	}
	pub fn is_unavailable(&self) -> bool {
		match self {
			MangaDexChapterAPI::Unavailable(_) => true,
			_ => false
		}
	}
	/// Converts a mangadexchapterapi into an Option for information.
	pub fn ok(self) -> Option<MangaDexChapterAPIOK> {
		match self {
			MangaDexChapterAPI::Ok(ok) => Some(*ok),
			_ => None
		}
	}
	pub fn delayed(self) -> Option<MangaDexChapterAPIDelayed> {
		match self {
			MangaDexChapterAPI::Delayed(delayed) => Some(*delayed),
			_ => None
		}
	}
	pub fn err(self) -> Option<Error> {
		match self {
			MangaDexChapterAPI::Error(err) => Some(err),
			_ => None
		}
	}
}

/// Generic error type
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Error {
	pub id: u32,
	pub message: String,
}
use std::fmt;
impl fmt::Display for Error {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		write!(fmt, "{} on id {}", self.message, self.id)
	}
}
impl std::error::Error for Error {}

#[cfg(test)]
pub mod tests {
	use super::MangaDexChapterAPI as API;
	use serde_json::from_str;
	type R = Result<(), Box<serde_json::Error>>;

	const TEST_DELETED: &str = include_str!("../testdata/chapter/1.json");
	const TEST_OK: &str = include_str!("../testdata/chapter/2.json");
	const TEST_OK_2: &str = include_str!("../testdata/chapter/42.json");
	const TEST_UNAVAILABLE: &str = include_str!("../testdata/chapter/614094.json");
	const TEST_ERROR: &str = include_str!("../testdata/chapter/9999999.json");

	/// Decoding of a deleted chapter
	#[test]
	fn it_decodes_a_deleted_chapter() -> R {
		let test_deleted: API = from_str(TEST_DELETED)?;
		assert!(test_deleted.is_deleted());
		eprintln!("{:?}", test_deleted);
		Ok(())
	}
	/// Decoding of an available chapter
	#[test]
	fn it_decodes_an_available_chapter() -> R {
		let test_avilable: API = from_str(TEST_OK)?;
		assert!(test_avilable.is_ok());
		eprintln!("{:?}", test_avilable);
		Ok(())
	}
	/// Decoding of another available chapter
	#[test]
	fn it_decodes_another_available_chapter() -> R {
		let test_avilable: API = from_str(TEST_OK_2)?;
		assert!(test_avilable.is_ok());
		eprintln!("{:?}", test_avilable);
		Ok(())
	}
	/// Decoding of an unavailable chapter
	#[test]
	fn it_decodes_an_unavailable_chapter() -> R {
		let test_unavilable: API = from_str(TEST_UNAVAILABLE)?;
		assert!(test_unavilable.is_unavailable());
		eprintln!("{:?}", test_unavilable);
		Ok(())
	}
	/// Decoding of a chapter that does not yet exist
	#[test]
	fn it_decodes_a_non_existing_chapter() -> R {
		let test_non_existing: API = from_str(TEST_ERROR)?;
		assert!(test_non_existing.is_error());
		eprintln!("{:?}", test_non_existing);
		Ok(())
	}
}
