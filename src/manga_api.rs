use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::collections::BTreeMap as Map;
use std::mem;
use crate::{Genre, Language};

/// Status of a manga from the mangadex manga api (/api/manga/47; .manga.status)
#[derive(Serialize_repr, Deserialize_repr, Clone, Debug, PartialEq)]
#[repr(u8)]
pub enum MangaDexAPIMangaStatus {
	Ongoing = 1,
	Completed = 2,
	Cancelled = 3,
	Hiatus = 4,
	#[serde(other)]
	Unknown = 0,
}

impl From<u8> for MangaDexAPIMangaStatus {
	fn from(v: u8) -> MangaDexAPIMangaStatus {
		if v < 5 {
			unsafe { mem::transmute(v) }
		} else {
			MangaDexAPIMangaStatus::Unknown
		}
	}
}

impl From<MangaDexAPIMangaStatus> for u8 {
	fn from(v: MangaDexAPIMangaStatus) -> u8 {
		v as u8
	}
}

/// Whether it's for over 18 or not (hentai).
#[derive(Debug, Serialize_repr, Deserialize_repr, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
#[repr(u8)]
pub enum Over18 {
	/// Not over 18 (boolean false).
	No = 0,
	/// Over 18 (boolean true).
	Yes = 1,
}
impl From<bool> for Over18 {
	fn from(b: bool) -> Self { if b { Over18::Yes } else { Over18::No } }
}
impl From<Over18> for bool {
	fn from(b: Over18) -> Self {
		match b {
			Over18::No => false,
			Over18::Yes => true,
		}
	}
}

impl Default for Over18 { fn default() -> Self { Over18::No } }
/// Manga API Links (/api/manga/47; .manga.links)
#[derive(Serialize, Deserialize, Default, Clone, Debug, PartialEq)]
pub struct MangaDexAPIMangaLinks {
	/// Bookwalker (prefix: https://bookwalker.jp/)
	#[serde(rename(deserialize = "bw"))]
	pub book_walker: Option<String>,
	/// MangaUpdates ID (prefix: https://www.mangaupdates.com/series.html?id=)
	#[serde(rename(deserialize = "mu"))]
	pub manga_updates: Option<String>,
	/// NovelUpdates slug (prefix: https://www.novelupdates.com/series/, postfix: /)
	#[serde(rename(deserialize = "nu"))]
	pub novel_updates: Option<String>,
	/// Amazon URL
	#[serde(rename(deserialize = "amz"))]
	pub amazon: Option<String>,
	/// CDJapan URL
	#[serde(rename(deserialize = "cdj"))]
	pub cd_japan: Option<String>,
	/// EbookJapan URL
	#[serde(rename(deserialize = "ebj"))]
	pub ebook_japan: Option<String>,
	/// MyAnimeList ID (prefix: https://myanimelist.net/manga/)
	#[serde(rename(deserialize = "mal"))]
	pub my_anime_list: Option<String>,
	/// Raw URL
	pub raw: Option<String>,
	/// Official English URL
	#[serde(rename(deserialize = "engtl"))]
	pub english_translation: Option<String>,
}

/// Manga information from the mangadex API (/api/manga/47; .manga)
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaDexAPIInfo {
	pub title: String,
	pub artist: String,
	pub author: String,
	pub status: MangaDexAPIMangaStatus,
	pub genres: Vec<Genre>,
	pub description: String,
	pub cover_url: String,
	pub last_chapter: String,
	#[serde(rename(deserialize = "lang_flag"))]
	pub lang: Language,
	pub hentai: Over18,
	pub links: Option<MangaDexAPIMangaLinks>,
}

/// Chapter information from the information from the mangadex manga api (/api/manga/47; .chapter[])
#[derive(Serialize, Deserialize, Default, Clone, Debug, PartialEq)]
pub struct MangaDexAPIMangaChapter {
	pub volume: String,
	pub chapter: String,
	pub title: String,
	#[serde(rename(deserialize = "lang_code"))]
	pub lang: Language,
	pub group_id: u32,
	pub group_id_2: u32,
	pub group_id_3: u32,
	pub group_name: String,
	pub group_name_2: Option<String>,
	pub group_name_3: Option<String>,
	pub timestamp: u64,
}

/// Manga data from the mangadex manga api (/api/manga/47; .)
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct MangaAPIData {
	/// Manga info
	pub manga: MangaDexAPIInfo,
	/// Chapter IDs info (can be undefined)
	#[serde(rename(deserialize = "chapter"), default = "Map::new")]
	pub chapters: Map<String, MangaDexAPIMangaChapter>,
}

/// Information from the mangadex manga api (/api/manga/47; .)
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "status")]
pub enum MangaDexMangaAPI {
	/// Whether the ID is gone (eg id 420) or doesn't exist (9999999) it makes no distinction.
	#[serde(rename(deserialize = "Manga ID does not exist."))]
	MangaIDDoesNotExist,
	/// Status is OK, data is fine.
	/// (box because clippy complains, lowercase for lint)
	#[serde(rename(deserialize = "OK"))]
	Ok(Box<MangaAPIData>),
}
impl MangaDexMangaAPI {
	pub fn is_ok(&self) -> bool {
		match self {
			MangaDexMangaAPI::Ok(_) => true,
			_ => false
		}
	}
	pub fn does_not_exist(&self) -> bool {
		match self {
			MangaDexMangaAPI::MangaIDDoesNotExist => true,
			_ => false,
		}
	}
}

#[cfg(test)]
pub mod tests {
	use super::*;
	use serde_json::from_str;
	type R = Result<(), Box<serde_json::Error>>;

	const TEST_MANGA: &str = include_str!("../testdata/manga/47.json");
	const TEST_NO_DATA: &str = include_str!("../testdata/manga/420.json");
	const TEST_01: &str = include_str!("../testdata/manga/1.json");
	const TEST_99: &str = include_str!("../testdata/manga/99.json");
	const TEST_39_000: &str = include_str!("../testdata/manga/39000.json");
	/// Decoding of the test (id 47) manga
	#[test]
	fn it_decodes_the_test_manga() -> Result<(), Box<serde_json::Error>> {
		let test_test_manga: MangaDexMangaAPI = from_str(TEST_MANGA)?;
		assert!(test_test_manga.is_ok());
		eprintln!("{:?}", test_test_manga);
		Ok(())
	}
	/// Decoding of a deleted (id 420) manga
	#[test]
	fn it_decodes_a_manga_without_data() -> R {
		let test_no_data: MangaDexMangaAPI = from_str(TEST_NO_DATA)?;
		assert!(test_no_data.does_not_exist());
		eprintln!("{:?}", test_no_data);
		Ok(())
	}
	/// Decoding of a large (id 1) manga
	#[test]
	fn it_decodes_a_large_manga() -> R {
		let test_01: MangaDexMangaAPI = from_str(TEST_01)?;
		assert!(test_01.is_ok());
		eprintln!("{:?}", test_01);
		Ok(())
	}
	/// Decoding of another (id 99) manga
	#[test]
	fn it_decodes_another_manga() -> R {
		let test_99: MangaDexMangaAPI = from_str(TEST_99)?;
		assert!(test_99.is_ok());
		eprintln!("{:?}", test_99);
		Ok(())
	}
	/// Decoding of a manga that lacks both chapters and links (id 35000)
	#[test]
	fn it_decodes_a_manga_without_chapters_or_links() -> R {
		let test_39000: MangaDexMangaAPI = from_str(TEST_39_000)?;
		assert!(test_39000.is_ok());
		eprintln!("{:?}", test_39000);
		Ok(())
	}
	/// Decoding of a non-manga is an error
	#[test]
	fn it_does_not_decode_a_non_manga() -> Result<(), MangaDexMangaAPI> {
		let test_non_manga: Result<MangaDexMangaAPI, serde_json::Error> = from_str("{\"status\":\"None\"}");
		if test_non_manga.is_err() {
			let err = test_non_manga.expect_err("Error not found");
			assert!(err.is_data());
			eprintln!("{:?}", err);
			eprintln!("{}", err);
			Ok(())
		} else {
			Err(test_non_manga.unwrap())
		}
	}
	/// Decoding of incomplete json is an error
	#[test]
	fn it_does_not_decode_unfinished_json() {
		let test_non_manga: Result<MangaDexMangaAPI, serde_json::Error> = from_str("{\"status\":");
		assert!(test_non_manga.is_err());
		let err = test_non_manga.unwrap_err();
		assert!(err.is_eof());
		eprintln!("{:?}", err);
	}
	/// Decoding of bad json is an error
	#[test]
	fn it_does_not_decode_bad_json() {
		let test_non_manga: Result<MangaDexMangaAPI, serde_json::Error> = from_str("{\"status\":}");
		assert!(test_non_manga.is_err());
		let err = test_non_manga.unwrap_err();
		assert!(err.is_syntax());
		eprintln!("{:?}", err);
	}
}
