extern crate serde;
extern crate serde_repr;

pub mod genres;
pub mod langs;
pub mod manga_api;
pub mod chapter_api;
mod set;

pub use genres::{Genre, GenreSet};
pub use langs::{Language, LanguageSet};
pub use manga_api::MangaDexMangaAPI;
pub use chapter_api::MangaDexChapterAPI;

use serde::{Deserialize, Serialize};
/// API response type to be funneled through.
/// Please don't actually use it.
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(untagged)]
pub enum APIResponse {
	Chapter(MangaDexChapterAPI),
	Manga(MangaDexMangaAPI)
}
impl APIResponse {
	pub fn is_chapter(&self) -> bool {
		match self {
			APIResponse::Chapter(_) => true,
			_ => false
		}
	}
	pub fn is_manga(&self) -> bool {
		!self.is_chapter()
	}
	pub fn chapter(self) -> Option<MangaDexChapterAPI> {
		match self {
			APIResponse::Chapter(ch) => Some(ch),
			_ => None
		}
	}
	pub fn chapter_ok(self) -> Option<chapter_api::MangaDexChapterAPIOK> {
		if let APIResponse::Chapter(MangaDexChapterAPI::Ok(ch)) = self {
			Some(*ch)
		} else {
			None
		}
	}
	pub fn manga(self) -> Option<MangaDexMangaAPI> {
		match self {
			APIResponse::Manga(mn) => Some(mn),
			_ => None
		}
	}
	pub fn manga_ok(self) -> Option<manga_api::MangaAPIData> {
		if let APIResponse::Manga(MangaDexMangaAPI::Ok(mng)) = self {
			Some(*mng)
		} else {
			None
		}
	}
}

#[cfg(test)]
pub mod api_response {
	const CHAPTER: &str = include_str!("../testdata/chapter/2.json");
	const MANGA: &str = include_str!("../testdata/manga/47.json");
	use super::APIResponse;
	use serde_json::from_str;
	type R = Result<(), Box<serde_json::Error>>;

	#[test]
	fn it_decodes_a_manga_generically() -> R {
		let manga: APIResponse = from_str(MANGA)?;
		assert!(manga.is_manga());
		eprintln!("{:?}", manga);
		Ok(())
	}
	#[test]
	fn it_decodes_a_chapter_generically() -> R {
		let chapter: APIResponse = from_str(CHAPTER)?;
		assert!(chapter.is_chapter());
		eprintln!("{:?}", chapter);
		Ok(())
	}
}
