
#[macro_export] macro_rules! make_set_type {
	($name:ident ($internal:ident) / $type:ident) => {
		use std::ops::*;
		#[derive(Clone, Copy, Default, PartialEq, PartialOrd, Eq, Ord)]
		pub struct $name($internal);
		impl $name {
			pub fn new() -> Self { $name(0) }
			pub fn has(self, other: $type) -> bool {
				let amp = self.0 & (1 << other as u8);
				amp != 0
			}
			pub fn contains(self, other: Self) -> bool {
				(self & other).0 != 0
			}
		}
		impl From<Vec<$type>> for $name {
			fn from(list: Vec<$type>) -> Self {
				let mut data = $name(0);
				for child in list { data += child; }
				data
			}
		}
		impl Not for $name {
			type Output = Self;
			fn not(self) -> Self {
				$name(!self.0)
			}
		}
		#[allow(clippy::suspicious_op_assign_impl)]
		// Somehow clippy finds this suspicious but not the other ones.
		impl AddAssign<$type> for $name {
			fn add_assign(&mut self, rhs: $type) {
				self.0 |= 1 << rhs as u8;
			}
		}
		impl SubAssign<$type> for $name {
			fn sub_assign(&mut self, rhs: $type) {
				self.0 &= !(1 << rhs as u8);
			}
		}
		impl BitOrAssign for $name {
			fn bitor_assign(&mut self, rhs: Self) {
				self.0 |= rhs.0;
			}
		}
		impl BitXor for $name {
			type Output = Self;
			fn bitxor(self, rhs: Self) -> Self {
				$name(self.0 ^ rhs.0)
			}
		}
		impl BitOr for $name {
			type Output = Self;
			fn bitor(self, rhs: Self) -> Self {
				$name(self.0 | rhs.0)
			}
		}
		impl BitAnd for $name {
			type Output = Self;
			fn bitand(self, rhs: Self) -> Self {
				$name(self.0 & rhs.0)
			}
		}
	};
}
